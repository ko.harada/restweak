#!/usr/bin/env python3
#-*-coding:utf-8-*-

import sys
import yaml
import requests
import jmespath
import logging

logger = logging.getLogger('restweak')


class RESTweak:
    # requests.request kwargs keys:
    # http://docs.python-requests.org/en/master/api/#requests.request
    request_kargs = (
        'params', 'data', 'json', 'headers', 'cookies', 'files', 'auth',
        'timeout', 'allow_redirects', 'proxies', 'verify', 'stream',
        'cert')

    def __init__(self, **kwargs):
        self.io = {}
        self.io.update(yaml.load(sys.stdin))
        kwargs = {k: kwargs[k] for k in kwargs if kwargs[k]}
        self.io.update(kwargs)
        logger.debug('input: {}\n'.format(self.io))
        self._input_filter()
        self._request()
        self._output_filter()
        self._rename_data()
        yaml.dump(self.io, stream=sys.stdout, default_flow_style=False)

    def _input_filter(self):
        if 'input_filter' in self.io:
            self.io['data'] = self._filter(
                self.io['input_filter'], self.io.get('data', None))

    def _output_filter(self):
        if 'output_filter' in self.io:
            self.io['data'] = self._filter(
                self.io['output_filter'], self.io.get('data', None))

    def _filter(self, filtr, data):
        if isinstance(filtr, list):
            for fil in filtr:
                data = jmespath.search(fil, data)
        else:
            data = jmespath.search(filtr, data)
        return data

    def _build_kwargs(self):
        kwargs = {}
        for key in self.request_kargs:
            if key in self.io:
                kwargs[key] = self.io[key]
        return kwargs

    def _repr_request(self, method, url, **kwargs):
        req = requests.Request(method, url, **kwargs)
        out = ""
        kargs = ['method', 'url']
        kargs.extend(self.request_kargs)
        for key in kargs:
            if getattr(req, key, False):
                out += "{}: {}\n".format(key, getattr(req, key))
        return out

    def _request(self):
        method, url = self.io['method'], self.io['url']
        kwargs = self._build_kwargs()

        logger.debug(self._repr_request(method, url, **kwargs))
        if self.io.get('dryrun', False):
            sys.exit(0)

        res = requests.request(method, url, **kwargs)
        logger.info('> %d %s\n' % (res.status_code, res.reason))
        if res.ok:
            logger.debug('body: %s\n' % (res.text))
            self.io['data'] = res.json()
        else:
            res.raise_for_status()

    def _rename_data(self):
        setdata = self.io.get('setdata', False)
        if setdata and 'data' in self.io:
            self.io[setdata] = self.io.pop('data')


def main(**kwargs):
    RESTweak(**kwargs)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        description='restweak.py: REST API tweak utility')
    parser.add_argument('-m', '--method', action="store")
    parser.add_argument('-u', '--url', action="store")
    parser.add_argument('-i', '--input_filter', action="store")
    parser.add_argument('-o', '--output_filter', action="store")
    parser.add_argument('-d', '--setdata', action="store",
        help="set data as [value] instead of 'data'")
    parser.add_argument('-v', '--verbose', action="store_true")
    parser.add_argument('-p', '--dryrun', action="store_true")
    kwargs = vars(parser.parse_args())

    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(message)s')
    handler.setFormatter(formatter)
    if kwargs['verbose']:
        logger.setLevel(logging.DEBUG)
        handler.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        handler.setLevel(logging.INFO)
    logger.addHandler(handler)

    main(**kwargs)
